;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; mf-icomplete.el
;;
;; This file contains variables and functions dedicated to customizing
;; minibuffer completion
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'icomplete)
(require 'mf-utils)

(defun mf/icomplete-styles ()
  "Select completion styles for icomplete"
  (setq-local completion-styles '(partial-completion initials flex)))

(defun mf/icomplete-minibuffer-truncate ()
  "Force icomplete minibuffer to remain 1 line high by enforcing
    `icomplete-prospects-height' to 1"
  (setq-local icomplete-prospects-height 1)
  (when (and (minibufferp)
             (bound-and-true-p icomplete-mode))
    (setq truncate-lines t)))

(defun mf/icomplete-format-completions (completions)
  "Filter COMPLETIONS candidates. To be used as filter return
  advice for `icomplete-completions'. Taken from icomplete-vertical."
  (save-match-data
    (let ((reformatted
           (cond
            ((string-match "^\\((.*)\\|\\[.*\\]\\)?{\\(\\(?:.\\|\n\\)*\\)}$" completions)
             (format "%s %s"
                     (or (match-string 1 completions) "")
                     (match-string 2 completions)))
            ((string-match "^\\([([]\\)\n" completions)
             (concat (match-string 1 completions)
                     (substring completions 2)))
            (t completions))))
      (mf/icomplete-display-string reformatted))))

(defun mf/icomplete-display-string (str)
  "Return the string which STR displays as.
This replaces portions of STR that have display properties with
the string they will display as. Taken from icomplete-vertical."
  (let ((len (length str)) (pos 0) chunks)
    (while (not (eq pos len))
      (let ((end (next-single-property-change pos 'display str len))
            (display (get-text-property pos 'display str)))
        (push (if (stringp display) display (substring str pos end))
              chunks)
        (setq pos end)))
    (apply #'concat (nreverse chunks))))

(advice-add 'icomplete-completions :filter-return #'mf/icomplete-format-completions)

(defconst mf/icomplete-ignore-buffers-regexp
  '("\\` " "^\\*")
  "List of regexps or functions matching buffer names to ignore")

(defun mf/icomplete-ignore-buffer (name)
  "Returns non-nil if buffer NAME should be ignored by `mf/icomplete-switch-to-buffer'"
  (or
   (mf/find-str-in-regexp-list name mf/icomplete-ignore-buffers-regexp)
   (rassq (get-buffer name) dired-buffers)))

(defun mf/icomplete-make-buffer-list (&optional frame)
  "Returns list of non-ignored buffer names. Inspired from Ido."
  (delq nil
    (mapcar
     (lambda (x)
       (let ((name (buffer-name x)))
         (if (not (or (mf/icomplete-ignore-buffer name)
                      (equal name (buffer-name (current-buffer)))))
             name)))
     (buffer-list frame))))

(defun mf/read-buffer-to-switch (prompt)
  "Read the name of a buffer to switch to, prompting with PROMPT."
  (let ((rbts-buffer-list (mf/icomplete-make-buffer-list (selected-frame))))
    (minibuffer-with-setup-hook
        (lambda ()
          (setq minibuffer-completion-table rbts-buffer-list)
          (if (and (boundp 'icomplete-with-completion-tables)
                   (listp icomplete-with-completion-tables))
              (set (make-local-variable 'icomplete-with-completion-tables)
                   (cons rbts-buffer-list
                         icomplete-with-completion-tables))))
      (read-buffer prompt (other-buffer (current-buffer))
                   (confirm-nonexistent-file-or-buffer)))))

(defun mf/icomplete-switch-to-buffer (buffer-or-name)
  "Custom switch to buffer"
  (interactive
   (list (mf/read-buffer-to-switch "Switch to buffer: ")))
  (switch-to-buffer buffer-or-name))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'mf-icomplete)
