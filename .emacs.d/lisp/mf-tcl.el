;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; mf-tcl.el
;;
;; This file contains variables and functions dedicated to customizing
;; Tcl mode
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'tcl)

(defconst mf/tcl-application "/home/fiorenm/.local/bin/tclsh8.6"
  "tclsh custom binary")

(defconst mf/tcl-keywords
  '("foreach_in_collection" "redirect")
  "List of Tcl keywords added to the `tcl-keyword-list' variable for highlighting.")

(defconst mf/tcl-builtins
  '("try" "on" "trap" "finally" "lsearch" "echo" "set_app_var"
    "get_pins" "get_cells" "get_ports" "get_nets" "get_timing_paths" "get_attribute"
    "get_clocks" "get_designs" "get_libs" "get_lib_cells" "get_lib_pins" "get_power_domains"
    "get_object_name" "get_supply_nets" "get_supply_ports" "get_bounds" "get_power_switches")
  "List of Tcl keywords added to the `tcl-builtin-list' variable for highlighting.")

(defun mf/tcl-update-keyword-lists ()
  "Update the default lists of highlighted Tcl keywords"
  (dolist (keyword mf/tcl-keywords)
    (add-to-list 'tcl-keyword-list keyword))
  (dolist (keyword mf/tcl-builtins)
    (add-to-list 'tcl-builtin-list keyword))
  (tcl-set-font-lock-keywords))

(defun mf/tcl-electric-tab (&optional arg)
  "Indent the current line or region, or insert a tab, or
complete. Inspired from vhdl-mode and tcl-dc-mode."
  (interactive "P")
  (progn
    (cond
     ;; If region is active, indent
     ((use-region-p)
      (indent-region (region-beginning) (region-end)))
     ;; If preceding char is a word and following char is blank complete
     ((and
       (memq (char-syntax (preceding-char)) '(?w ?_))
       (not (memq (char-syntax (following-char)) '(?w ?_))))
      (let ((case-fold-search t)
            (case-replace nil)
            (current-syntax-table (syntax-table))
            (hippie-expand-only-buffers
             (or (and (boundp 'hippie-expand-only-buffers)
                      hippie-expand-only-buffers)
                 '(tcl-mode))))
        (set-syntax-table tcl-mode-syntax-table)
        (mf/tcl-expand-abbrev arg)
        (set-syntax-table current-syntax-table)))
     ;; If in middle of word, indent
     ((and
       (memq (char-syntax (preceding-char)) '(?w ?_))
       (memq (char-syntax (following-char)) '(?w ?_)))
      (tcl-indent-command))
     ;; If beyond indent point, go back
     ((> (current-column) (current-indentation))
      (tab-to-tab-stop))
     ;; If last command was electric-tab, dedent
     ((and (eq last-command 'mf/tcl-electric-tab)
           (/= 0 (current-indentation)))
      (backward-delete-char-untabify tcl-indent-level nil))
     ;; Finally, indent
     (t (tcl-indent-command))
    (setq this-command 'mf/tcl-electric-tab))
    (indent-according-to-mode)))

;; function for expanding abbrevs and dabbrevs
(defalias 'mf/tcl-expand-abbrev
  (make-hippie-expand-function
   '(try-expand-dabbrev
     try-expand-dabbrev-all-buffers)))

;; Tab config
(setq tcl-tab-always-indent t)
(add-hook 'tcl-mode-hook (lambda () (setq tab-always-indent nil)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'mf-tcl)
