;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; mf-rst.el
;;
;; This file contains variables and functions dedicated to customizing
;; reSt mode
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'rst)

(defun mf/rst-emphasize (emph)
  "Insert emphasis around selection"
  (interactive (list (completing-read "Char: " (list "bold" "it" "code"))))
  (let ((mf-rst-char (cond ((equal emph "bold") "**")
                           ((equal emph "it")   "*")
                           ((equal emph "code") "``")
                           (t ""))))
    (if (region-active-p)
        (let* ((beg (region-beginning))
               (end (region-end))
               (str (buffer-substring beg end)))
          (delete-region beg end)
          (insert (concat mf-rst-char str mf-rst-char))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'mf-rst)
