;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; mf-isearch.el
;;
;; This file contains variables and functions dedicated to customizing
;; search and replace features
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'hi-lock)

(defun mf/isearch-highlight (face)
  "Invoke highlight-phrase from within isearch"
  (interactive (list (hi-lock-read-face-name)))
  (let ((regexp (if isearch-regexp
                    isearch-string
                  (regexp-quote isearch-string))))
    (highlight-phrase regexp face)
    (isearch-done)))

(defun mf/isearch-mark-and-exit ()
  "Mark the current search string and exit the search."
  (interactive)
  (push-mark isearch-other-end t 'activate)
  (setq deactivate-mark nil)
  (isearch-done))

(defun mf/isearch-kill ()
  "Kill the current matching string into kill ring"
  (interactive)
  (kill-region isearch-other-end (point)))

(defun mf/isearch-kill-to-register ()
  "Kill the current matching string, copy it to register, and go to next match"
  (interactive)
  (append-to-register ?i isearch-other-end (point) t)
  (append-to-register ?i isearch-other-end (point) t)
  (isearch-repeat-forward))

(defun mf/isearch-init-register ()
  "Initializes the isearch register"
  (interactive)
  (set-register ?i nil))

(defun mf/isearch-selection ()
  "Use selected region as the isearch text"
  (when mark-active
    (let ((region (funcall region-extract-function nil)))
      (goto-char (region-beginning))
      (deactivate-mark)
      (isearch-update)
      (isearch-yank-string region))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'mf-isearch)
