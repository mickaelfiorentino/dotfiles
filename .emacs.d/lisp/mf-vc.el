;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; mf-vc.el
;;
;; This file contains variables and functions dedicated to customizing
;; Version Control. Mostly taken from Prot.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'vc)
(require 'log-edit)

(defvar mf/vc-current-window-configuration nil
  "Current window configuration for use with Log Edit.")

(defvar mf/vc-current-window-configuration-point nil
  "Point in current window configuration for use with Log Edit.")

(defun mf/vc-store-window-configuration ()
  "Store window configuration before calling `vc-start-logentry'."
  (setq mf/vc-current-window-configuration (current-window-configuration))
  (setq mf/vc-current-window-configuration-point (point)))

(defun mf/vc-restore-window-configuration ()
  "Restore window configuration to the pre Log Edit state."
  (when mf/vc-current-window-configuration
    (set-window-configuration mf/vc-current-window-configuration))
  (when mf/vc-current-window-configuration-point
    (goto-char mf/vc-current-window-configuration-point)))

(defun mf/vc-set-window-configuration ()
  "Set VC window configuration with Diff, Log Edit, and Log Files buffers."
  (let ((buffer (get-buffer "*vc-log*")))
    (with-current-buffer (if (buffer-live-p buffer)
                             buffer
                           (window-buffer (get-mru-window)))
      (delete-other-windows)
      (when (ignore-errors ; This condition saves us from error on new repos
              (process-lines "git" "--no-pager" "diff-index" "-p" "HEAD" "--"))
        (log-edit-show-diff))
      (set-buffer buffer)
      (log-edit-show-files)
      (other-window -1))))

(defun mf/vc-kill-log-edit ()
  "Local hook to restore windows when Log Edit buffer is killed."
  (when (or (derived-mode-p 'log-edit-mode)
            (derived-mode-p 'diff-mode))
    (add-hook 'kill-buffer-hook #'mf/vc-restore-window-configuration 0 t)))

(defvar mf/vc-pre-log-edit-hook nil
  "Hook that runs before `vc-start-logentry'.")

(defun mf/vc-pre-log-edit (&rest _)
  "Run `mf/vc-pre-log-edit-hook'. To be used as advice before `vc-start-logentry'."
  (run-hooks 'mf/vc-pre-log-edit-hook))

(defvar mf/vc-post-log-edit-hook nil
  "Hook that runs after `vc-start-logentry'.")

(defun mf/vc-post-log-edit (&rest _)
  "Run `mf/vc-post-log-edit-hook'. To be used as advice after `vc-start-logentry'."
  (run-hooks 'mf/vc-post-log-edit-hook))

(defvar mf/vc-log-edit-done-hook nil
  "Hook that runs after `log-edit-done'.")

(defun mf/vc-log-edit-done (&rest _)
  "Run `mf/vc-log-edit-done-hook'. To be used as advice before `log-edit-done'."
  (run-hooks 'mf/vc-post-log-edit-hook))

(advice-add #'vc-start-logentry :before #'mf/vc-pre-log-edit)
(add-hook 'mf/vc-pre-log-edit-hook #'mf/vc-store-window-configuration)

(advice-add #'vc-start-logentry :after #'mf/vc-post-log-edit)
(add-hook 'mf/vc-post-log-edit-hook #'mf/vc-set-window-configuration)
(remove-hook 'log-edit-hook #'log-edit-show-files)

(advice-add #'log-edit-done :after #'mf/vc-log-edit-done)
(add-hook 'mf/vc-log-edit-done-hook #'mf/vc-restore-window-configuration)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'mf-vc)
