;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; mf-speedbar.el
;;
;; This file contains variables and functions dedicated to customizing
;; the speedbar
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'speedbar)

(defconst mf/speedbar-buffer-name "*SPEEDBAR*"
  "Name of the speedbar buffer")

(defun mf/speedbar-toggle ()
  "Open/Closes the speedbar window"
  (interactive)
  (if (buffer-live-p speedbar-buffer)
      (kill-buffer speedbar-buffer)
    (mf/speedbar-open-window)))

(defun mf/speedbar-close-window ()
  "Function that kills the speedbar buffer and its window"
  (interactive)
  (if (buffer-live-p speedbar-buffer)
      (kill-buffer speedbar-buffer)))

(defun mf/speedbar-open-window ()
  "Enable speedbar. Inspired from speedbar-frame-mode and sr-speedbar."
  (interactive)
  (setq speedbar-buffer (get-buffer-create mf/speedbar-buffer-name))

  (with-current-buffer speedbar-buffer
    (speedbar-mode)
    (buffer-disable-undo speedbar-buffer)
    (set (make-local-variable 'auto-hscroll-mode) nil)
    (pop-to-buffer speedbar-buffer))

  (setq speedbar-frame (selected-frame)
        dframe-attached-frame (selected-frame)
        speedbar-select-frame-method 'attached
        speedbar-verbosity-level 0
        speedbar-last-selected-file nil)

  (speedbar-reconfigure-keymaps)
  (speedbar-update-contents)
  (speedbar-set-timer 1)

  (add-hook 'speedbar-before-visiting-file-hook 'mf/speedbar-visiting-hook)
  (add-hook 'speedbar-before-visiting-tag-hook 'mf/speedbar-visiting-hook)
  (add-hook 'speedbar-visiting-file-hook 'mf/speedbar-visiting-hook)
  (add-hook 'speedbar-visiting-tag-hook 'mf/speedbar-visiting-hook)
  (add-hook 'kill-buffer-hook 'mf/speedbar-killing-hook))

(defun mf/speedbar-visiting-hook ()
  "Function that hooks to `speedbar-visiting-file/tag-hooks'"
  (select-window (previous-window)))

(defun mf/speedbar-killing-hook ()
  "Function that hooks to `kill-buffer-hook'"
  (when (eq (current-buffer) speedbar-buffer)
    (setq speedbar-frame nil
          dframe-attached-frame nil
          speedbar-buffer nil)
    (speedbar-set-timer nil)
    (remove-hook 'speedbar-before-visiting-file-hook 'mf/speedbar-visiting-hook)
    (remove-hook 'speedbar-before-visiting-tag-hook 'mf/speedbar-visiting-hook)
    (remove-hook 'speedbar-visiting-file-hook 'mf/speedbar-visiting-hook)
    (remove-hook 'speedbar-visiting-tag-hook 'mf/speedbar-visiting-hook)
    (remove-hook 'kill-buffer-hook 'mf/speedbar-killing-hook)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'mf-speedbar)
