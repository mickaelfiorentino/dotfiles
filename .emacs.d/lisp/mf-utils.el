;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; mf-utils.el
;;
;; This file contains variables and functions for custom emacs utilities
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun mf/toggle-mouse-speed()
  "Toggle `mouse-wheel-progressive-speed' value"
  (interactive)
  (if mouse-wheel-progressive-speed
      (setq mouse-wheel-progressive-speed nil)
    (setq mouse-wheel-progressive-speed t)))

(defun mf/copy-line (arg)
  "Copy arg lines to the kill ring"
  (interactive "p")
  (kill-ring-save
   (line-beginning-position)
   (if (= 1 arg)
       (line-end-position)
     (line-beginning-position (+ 1 arg)))))

(defun mf/duplicate-line ()
  "Duplicate current line"
  (interactive)
  (mf/copy-line 1)
  (move-end-of-line nil)
  (newline)
  (yank))

(defun mf/filename-to-kill-ring ()
  "Add filename to the kill ring and display in the
minibuffer. If called with a prefix argument return only the
filename without its path"
  (interactive)
  (let ((name
         (cond ((equal (car current-prefix-arg) 4) buffer-file-truename)
               ((equal (car current-prefix-arg) 16) (buffer-name))
               (t (buffer-file-name)))))
    (kill-new name)
    (message "%s" name)))

(defun mf/toggle-frame-split-windows ()
  "Toggles between vertical and horizontal split of windows ina
frame. Must be two windows already opened"
  (interactive)
  ;; Error if the windows count is not exactly 2
  (unless (= (count-windows) 2)
    (error (format "You can only toggle a frame split when it is composed of 2 windows (%d here)." (count-windows))))
  ;; Determine the current split and switch
  (let ((split-vertically-p (window-combined-p nil))
        (this-buffer (window-buffer (selected-window))))
    (delete-window (selected-window))
    (if split-vertically-p
        (split-window-horizontally)
      (split-window-vertically))
    (switch-to-buffer this-buffer)))

(defun mf/make-frame-maximized ()
  "Make a new frame in full screen"
  (interactive)
  (if (display-graphic-p)
      (set-frame-parameter (make-frame) 'fullscreen 'maximized)
    (select-frame (make-frame))))

(defun mf/find-str-in-regexp-list (str regexp-list)
  "Returns non-nil if STR is found in the list REGEXP-LIST"
  (let ((case-fold-search nil))
    (catch 'done
      (dolist (regexp regexp-list)
        (when (string-match regexp str)
          (throw 'done t))))))

(defun mf/set-buffer-in-special-mode (buffer alist)
  "Set BUFFER in special-mode. To be used with `display-buffer-alist'"
  (message "%s" (buffer-name buffer))
  (with-current-buffer buffer
    (special-mode)))

(defun mf/major-mode-of (&optional buffer-or-name)
  "Return the major mode symbol of the specified BUFFER-OR-NAME.
If not specified (or nil) return the major mode of the current buffer."
  (if buffer-or-name
      (with-current-buffer buffer-or-name
        major-mode)
    major-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'mf-utils)
