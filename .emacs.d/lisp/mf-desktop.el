;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; mf-desktop.el
;;
;; This file contains variables and functions dedicated to customizing
;; desktop accesses and manipulation
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'desktop)

(defun mf/desktop-load (desktop-name)
  "Load a desktop from the default desktop directory"
  (interactive
   (list (completing-read "Desktop name: "
                          (remove "." (remove ".." (directory-files desktop-path))))))
  (let ((desktop-dir (file-name-as-directory (expand-file-name desktop-name desktop-path))))
    (desktop-change-dir desktop-dir)))

(defun mf/desktop-create (desktop-name)
  "Create a new desktop in `desktop-path' and load it"
  (interactive "sDesktop name: ")
  (let ((desktop-dir (file-name-as-directory (expand-file-name desktop-name desktop-path))))
    (if (file-directory-p desktop-dir)
       (message "Desktop %s already exists" desktop-dir)
      (make-directory desktop-dir)
      (desktop-save desktop-dir)
      (message "New desktop created in %s" desktop-dir))))


(defun mf/desktop-remove (desktop-name)
  "Remove a desktop directory from `desktop-path'"
  (interactive
   (list (completing-read "Desktop name: "
                          (remove "." (remove ".." (directory-files desktop-path))))))
  (let ((desktop-dir (file-name-as-directory (expand-file-name desktop-name desktop-path))))
    (if (file-directory-p desktop-dir)
        (progn
          (delete-directory desktop-dir t t)
          (message "Desktop %s deleted" desktop-dir))
      (message "Desktop %s does not exist" desktop-dir))))

(defun mf/desktop-rename (old-name &optional new-name)
  "Rename a desktop directory from `desktop-path'"
  (interactive
   (list (completing-read "Old desktop name: "
                          (remove "." (remove ".." (directory-files desktop-path))))))
  (let ((newname (or new-name  ; use second arg, if non-nil
                     (read-from-minibuffer "New desktop name: "))))
    (let ((old-desktop (expand-file-name old-name desktop-path))
          (new-desktop (expand-file-name newname desktop-path)))
      (if (file-exists-p old-desktop)
          (progn
            (rename-file old-desktop new-desktop)
            (message "Renamed desktop %s to %s" old-desktop new-desktop))
        (message "Desktop %s does not exist" old-desktop)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'mf-desktop)
