;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; mf-dired.el
;;
;; This file contains variables and functions dedicated to customizing
;; Dired file manager
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'dired)
(require 'find-dired)

(defun mf/dired-open-external-file ()
  "Open files with xdg-open using & in dired"
  (interactive)
  (let* ((file (dired-get-filename nil t)))
    (call-process "xdg-open" nil 0 nil file)))

(defun mf/dired-find-file-other-frame ()
  "Visit file at point in an other frame"
  (interactive)
  (let ((window (display-buffer-use-some-frame (find-file-noselect (dired-get-filename nil t))
                                               (append (list '(inhibit-same-window . t))))))
    (select-window window)))

(defun mf/find-name-dired-current (pattern)
  "Search current dired directory recursively for files matching
PATTERN. Based on `find-name-dired'."
  (interactive "sFilename (wildcard): ")
  (let ((dir (dired-current-directory)))
    (find-name-dired dir pattern)))

(defun mf/find-grep-dired-current (pattern)
  "Find files in current dired directory that contain matches for
REGEXP. Based on `find-grep-dired'."
  (interactive "sGrep (wildcard): ")
  (let ((dir (dired-current-directory)))
    (find-dired dir
                (concat "-type f -follow -exec " grep-program " " find-grep-options " -e "
                        (shell-quote-argument pattern)
                        " "
                        (shell-quote-argument "{}")
                        " "
                        (shell-quote-argument ";")))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'mf-dired)
