;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; mf-tramp.el
;;
;; This file contains variables and functions dedicated to customizing
;; Tramp
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'tramp)

(defconst mf/tramp-plink-method
  '("mf-plink"
    (tramp-login-program "c:/Software/Putty/PLINK.EXE")
    (tramp-login-args
     (("-load")
      ("%h")
      ("-t")
      ("\"")
      ("env 'TERM=dumb' 'PROMPT_COMMAND=' 'PS1=#$ '")
      ("%l")
      ("\"")))
    (tramp-remote-shell "/bin/bash")
    (tramp-remote-shell-login ("-l"))
    (tramp-remote-shell-args  ("-c"))
    (tramp-default-port       22))
  "Method to use plink")

(defconst mf/tramp-default-method
  (if (equal window-system 'w32)
      "mf-plink"
    "sshx")
  "Tramp default method")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'mf-tramp)
