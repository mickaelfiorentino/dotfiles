;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; mf-occur.el
;;
;; This file contains variables and functions dedicated to customizing
;; occur (grep) features
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'replace)
(require 'hi-lock)

(defun mf/multi-occur-in-matching-buffers (regexp &optional allbufs)
  "Show lines matching REGEXP in all buffers"
  (interactive (occur-read-primary-args))
  (multi-occur-in-matching-buffers "." regexp allbufs))

(defun mf/occur-flush-lines (regexp)
  "Make occur buffer editable and flush lines matching regexp"
  (interactive (list (read-regexp "Flush lines matching regexp")))
  (occur-edit-mode)
  (flush-lines regexp)
  (occur-cease-edit))

(defun mf/occur-keep-lines (regexp)
  "Make occur buffer editable and keep only lines matching regexp"
  (interactive (list (read-regexp "Keep only lines matching regexp")))
  (occur-edit-mode)
  (keep-lines regexp)
  (occur-cease-edit))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'mf-occur)
