;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; mf-org.el
;;
;; This file contains variables and functions dedicated to customizing
;; Org mode
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'org)

(defun mf/org-babel-execute-noask ()
  "Execute source block without asking for confirmation"
  (interactive)
  (setq org-confirm-babel-evaluate nil)
  (org-babel-execute-src-block)
  (setq org-confirm-babel-evaluate t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'mf-org)
