;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Emacs initialization file
;;
;; see config.org
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'org)
(if (file-exists-p (expand-file-name "config.org" (getenv "USER_EMACS_DIRECTORY")))
    (setq-default user-emacs-directory (getenv "USER_EMACS_DIRECTORY"))
  (setq-default user-emacs-directory "~/.emacs.d/"))
(org-babel-load-file (expand-file-name "config.org" user-emacs-directory))
