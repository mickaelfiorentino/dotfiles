#+TITLE: Emacs initialization file
#+AUTHOR: Mickael Fiorentino
#+EMAIL: mickael.fiorentino@mailbox.org

* Introduction

Configuration that relies only on default features of Emacs-27 (meant to be used in environments where internet is not available for package update). Should be compatible with Linux & Windows, GUI & Terminals. Anyone should feel free to reuse anything of interest.
Mickael Fiorentino - <2021-04-13 Tue>

* Basics
** Defaults

#+begin_src emacs-lisp
(setq-default
load-prefer-newer t                                 ; Prefer newer file by default
inhibit-startup-screen t                            ; Disable start-up screen
initial-scratch-message ""                          ; Empty the initial *scratch* buffer
visible-bell 1                                      ; Please don't beep at me
indent-tabs-mode nil                                ; Stop using tabs to indent
tab-width 4                                         ; Set width for tabs
line-spacing 4                                      ; Better readability
tab-always-indent 'complete                         ; Tabs perform completion
make-backup-files nil                               ; No backup files
auto-save-default nil                               ; No auto-save
auto-save-list-file-prefix nil                      ; Do not record sessions
mouse-yank-at-point t                               ; Yank at point rather than pointer
fill-column 80                                      ; Set width for automatic line breaks
gc-cons-threshold            50000000               ; Garbage collection size : 50Mb
large-file-warning-threshold 100000000              ; File size limit warning : 100Mb
delete-by-moving-to-trash t                         ; Delete files to trash
require-final-newline t                             ; May prevent some problems
display-time-format "%H:%M"                         ; Format the time string
scroll-conservatively most-positive-fixnum          ; Always scroll by one line
scroll-preserve-screen-position t                   ; Preserve screen position
scroll-margin 0                                     ; Add a margin when scrolling vertically
mouse-wheel-scroll-amount '(1 ((shift) . 1))        ; Mouse scroll by 1 line
mouse-wheel-progressive-speed t                     ; Keep same speed
select-enable-clipboard t                           ; Merge system's and Emacs' clipboard
show-trailing-whitespace nil                        ; Display trailing whitespaces
uniquify-buffer-name-style 'forward                 ; uniquify buffer names
auto-window-vscroll nil                             ; Avoid lag
x-stretch-cursor t)                                 ; Stretch cursor to the glyph width
#+end_src

#+begin_src emacs-lisp
(column-number-mode t)                               ; Column number next to line number
(global-subword-mode t)                              ; Iterate through CamelCase words
(delete-selection-mode nil)                          ; Replace region when inserting text
(global-auto-revert-mode t)                          ; Automatically revert buffer from file
(display-time-mode t)                                ; Enable time in the mode-line
(show-paren-mode t)                                  ; Show matching parenthesis
(set-default-coding-systems 'utf-8)                  ; Default to utf-8 encoding
(prefer-coding-system 'utf-8)                        ; Prefer utf-8
(fset 'yes-or-no-p 'y-or-n-p)                        ; Replace yes/no prompts with y/n
(add-hook 'before-save-hook 'whitespace-cleanup)     ; Delete whitespaces before saving
#+end_src

#+begin_src emacs-lisp
(unless (display-graphic-p)
  (xterm-mouse-mode))
#+end_src

#+begin_src emacs-lisp
(setq register-separator ?+)
(set-register register-separator "\n")
#+end_src

** Load Path

#+begin_src emacs-lisp
(add-to-list 'load-path (concat user-emacs-directory "lisp"))
(let ((default-directory (concat user-emacs-directory "lisp")))
  (normal-top-level-add-subdirs-to-load-path))
#+end_src

** Global Keybindings

#+begin_src emacs-lisp
(require 'mf-utils)
(global-set-key (kbd "RET")             'newline-and-indent)
(global-set-key (kbd "C-x C-b")         'ibuffer)
(global-set-key (kbd "M-g")             'goto-line)
(global-set-key (kbd "M-u")             'undo)
(global-set-key (kbd "M-k")             'kill-this-buffer)
(global-set-key (kbd "C-f")             'forward-sexp)
(global-set-key (kbd "C-b")             'backward-sexp)
(global-set-key (kbd "C-c t")           'org-time-stamp)
(global-set-key (kbd "C-c q")           'quick-calc)
(global-set-key (kbd "C-c u")           'upcase-dwim)
(global-set-key (kbd "C-c l")           'downcase-dwim)
(global-set-key (kbd "C-c a")           'align-regexp)
(global-set-key (kbd "C-c f")           #'mf/filename-to-kill-ring)
(global-set-key (kbd "C-c w")           #'mf/copy-line)
(global-set-key (kbd "C-d")             #'mf/duplicate-line)
(global-set-key (kbd "C-q")             'repeat)
(global-set-key (kbd "<f2>")            'split-window-vertically)
(global-set-key (kbd "<f3>")            'split-window-horizontally)
(global-set-key (kbd "<f1>")            'delete-window)
(global-set-key (kbd "<C-f1>")          'delete-other-windows)
(global-set-key (kbd "<f4>")            #'mf/make-frame-maximized)
(global-set-key (kbd "<C-tab>")         'other-window)
(global-set-key (kbd "<next>")          'end-of-buffer)
(global-set-key (kbd "<prior>")         'beginning-of-buffer)
#+end_src

** Minibuffer history

#+begin_src emacs-lisp
(savehist-mode 1)
(setq savehist-file (concat user-emacs-directory "savehist"))
(setq history-length 30000)
(setq history-delete-duplicates t)
#+end_src

** Backups

#+begin_src emacs-lisp
(setq save-place-file (concat user-emacs-directory "places")
    backup-directory-alist `(("." . ,(concat user-emacs-directory "backups"))))
#+end_src

** Visual line mode

#+begin_src emacs-lisp
(add-hook 'text-mode-hook  'turn-on-visual-line-mode)
(add-hook 'woman-mode-hook 'turn-on-visual-line-mode)
(add-hook 'man-mode-hook   'turn-on-visual-line-mode)
(add-hook 'view-mode-hook  'turn-on-visual-line-mode)
(add-hook 'help-mode-hook  'turn-on-visual-line-mode)
#+end_src

** Advices

#+begin_src emacs-lisp
(require 'mf-advices)
#+end_src

* TAS

mf-tas config is only available in TAS

#+begin_src emacs-lisp
(require 'mf-tas nil 'noerror)
#+end_src

* Windows & Frames
** Frames

#+begin_src emacs-lisp
(when (display-graphic-p)
  (fringe-mode 0)
  (tool-bar-mode -1)
  (menu-bar-mode -1)
  (scroll-bar-mode -1)
  (put 'suspend-frameme 'disabled t)
  (add-to-list 'initial-frame-alist '(fullscreen . maximized)))
(setq help-window-select t)
(setq window-combination-resize t)
(setq split-width-threshold nil)
#+end_src

#+begin_src emacs-lisp
(setq frame-title-format
   '(buffer-file-name "%b - %f"             ; File buffer
     (dired-directory dired-directory       ; Dired buffer
      (revert-buffer-function "%b"          ; Buffer Menu
       ("%b - Dir: " default-directory))))) ; Plain buffer
#+end_src

#+begin_src emacs-lisp
(require 'mf-utils)
(global-set-key (kbd "C-z") ctl-x-5-map)
(define-key ctl-x-5-map (kbd "C-z") #'mf/make-frame-maximized)
(define-key ctl-x-5-map (kbd "z")   #'make-frame-command)
(define-key ctl-x-5-map (kbd "o")   #'other-frame)
(define-key ctl-x-5-map (kbd "s")   #'mf/toggle-frame-split-windows)
(define-key ctl-x-5-map (kbd "=")   #'toggle-frame-maximized)
(define-key ctl-x-5-map (kbd "-")   #'iconify-frame)
#+end_src

** Windows organization

#+begin_src emacs-lisp
(require 'mf-utils)
(setq switch-to-buffer-obey-display-actions t)
(setq display-buffer-alist
      '(
        (".*\\*Completions.*"
         (display-buffer-in-side-window)
         (window-height . 0.3)
         (side . bottom)
         (slot . 0)
         (window-parameters . ((no-other-window . t))))

        (".*\\*buffer-selection.*"
         (display-buffer-in-side-window)
         (window-height . 0.3)
         (side . bottom)
         (slot . 0)
         (window-parameters . ((no-other-window . t))))

        (".*\\*Bookmark List.*"
         (display-buffer-in-side-window)
         (window-height . 0.3)
         (side . bottom)
         (slot . 0)
         (window-parameters . ((no-other-window . t))))

        (".*\\*Occur.*"
         (display-buffer-in-side-window)
         (window-height . 0.3)
         (side . bottom)
         (slot . 0)
         (window-parameters . ((no-other-window . t))))

        (".*\\*e?shell.*"
         (display-buffer-in-side-window)
         (window-height . 0.3)
         (side . bottom)
         (slot . 0)
         (window-parameters . ((no-other-window . t))))

        (".*\\*Async Shell.*"
         (display-buffer-in-side-window)
         (window-height . 0.3)
         (side . bottom)
         (slot . 0)
         (window-parameters . ((no-other-window . t))))

        (".*\\*Python.*"
         (display-buffer-in-side-window)
         (window-height . 0.3)
         (side . bottom)
         (slot . 0)
         (window-parameters . ((no-other-window . t))))

        (".*\\*inferior-tcl.*"
         (display-buffer-in-side-window)
         (window-height . 0.3)
         (side . bottom)
         (slot . 0)
         (window-parameters . ((no-other-window . t))))

        (".*\\*Cleartool.*"
         (display-buffer-in-side-window)
         (window-height . 0.3)
         (side . bottom)
         (slot . 0)
         (window-parameters . ((no-other-window . t))))

        (".*\\*xref.*"
         (display-buffer-in-side-window)
         (window-height . 0.3)
         (side . bottom)
         (slot . 0)
         (window-parameters . ((no-other-window . t))))

        (".*\\*Find.*"
         (display-buffer-in-side-window)
         (window-height . 0.3)
         (side . bottom)
         (slot . 0)
         (window-parameters . ((no-other-window . t))))

        (".*\\*grep.*"
         (display-buffer-in-side-window)
         (window-height . 0.3)
         (side . bottom)
         (slot . 0)
         (window-parameters . ((no-other-window . t))))

        (".*\\*vc-git.*"
         (display-buffer-in-side-window)
         (window-height . 0.3)
         (side . bottom)
         (slot . 0)
         (window-parameters . ((no-other-window . t))))

        (".*\\*eww bookmarks.*"
         (display-buffer-in-side-window)
         (window-height . 0.3)
         (side . bottom)
         (slot . 0)
         (window-parameters . ((no-other-window . t))))

        (".*\\*SPEEDBAR.*"
         (display-buffer-in-side-window)
         (window-width . 0.1)
         (side . left)
         (slot . 0)
         (window-parameters . ((no-other-window . t))))

        (".*\\*Help.*"
         (display-buffer-in-side-window)
         (window-width . 0.3)
         (side . left)
         (slot . 0)
         (window-parameters . ((no-other-window . t))))

        (".*\\*info.*"
         (display-buffer-in-side-window)
         (window-width . 0.3)
         (side . left)
         (slot . 0)
         (window-parameters . ((no-other-window . t))))

        (".*\\*Woman.*"
         (display-buffer-in-side-window)
         (window-width . 0.3)
         (side . left)
         (slot . 0)
         (window-parameters . ((no-other-window . t))))

        (".*\\*Man.*"
         (display-buffer-in-side-window)
         (window-width . 0.3)
         (side . left)
         (slot . 0)
         (window-parameters . ((no-other-window . t))))
        ))
#+end_src

* Buffers
** Minibuffer

- This allows to apply commands on commands...
  #+begin_src emacs-lisp
  (setq enable-recursive-minibuffers t)
  (minibuffer-depth-indicate-mode)
  #+end_src

- Icomplete config
  #+begin_src emacs-lisp
  (setq icomplete-delay-completions-threshold 100)
  (setq icomplete-max-delay-chars 2)
  (setq icomplete-compute-delay 0.2)
  (setq icomplete-show-matches-on-no-input t)
  (setq icomplete-hide-common-prefix nil)
  (setq icomplete-separator (propertize "  -  " 'face 'shadow))
  (setq icomplete-with-completion-tables t)
  (setq icomplete-tidy-shadowed-file-names t)
  (fido-mode t)
  (with-eval-after-load "icomplete"
    (define-key icomplete-minibuffer-map (kbd "<backtab>") 'icomplete-force-complete)
    (define-key icomplete-minibuffer-map (kbd "C-v") 'exit-minibuffer))
  #+end_src

- Custom icomplete config
  #+begin_src emacs-lisp
  (require 'mf-icomplete)
  (add-hook 'icomplete-minibuffer-setup-hook #'mf/icomplete-minibuffer-truncate)
  (add-hook 'icomplete-minibuffer-setup-hook #'mf/icomplete-styles)
  (global-set-key (kbd "<menu>") #'mf/icomplete-switch-to-buffer)
  (global-set-key (kbd "<apps>") #'mf/icomplete-switch-to-buffer)
  #+end_src

** Buffer Selection

#+begin_src emacs-lisp
(require 'bs)
(add-to-list 'bs-configurations
             '("dired" nil nil nil
               (lambda (buf)
                 (with-current-buffer buf
                   (not (eq major-mode 'dired-mode)))) nil))
(global-set-key (kbd "<C-menu>") #'bs-show)
(global-set-key (kbd "<C-apps>") #'bs-show)
(setq bs-default-configuration "dired")
#+end_src

** Ibuffer

#+begin_src emacs-lisp
(require 'ibuffer)
(setq ibuffer-saved-filter-groups
  (quote (("Default"
          ("Emacs"    (or
                       (name . "^config.org$")
                       (mode . emacs-lisp-mode)))
          ("Dired"    (mode . dired-mode))
          ("Specials" (or
                       (name . "^\\*scratch\\*$")
                       (name . "^\\*Messages\\*$")
                       (name . "^\\*xref\\*$")
                       (name . "^\\*info\\*$")
                       (name . "^\\*Completions\\*$")
                       (name . "^\\*Occur\\*$")
                       (name . "^\\*Woman-Log\\*$")
                       (name . "^\\*Dired log\\*$")
                       (name . "^\\*Bookmark List\\*$")
                       (name . "^\\*Help\\*$")))
          ("VC"       (or
                       (name . "^\\*vc.*\\*$")
                       (name . "^\\*log-edit-files\\*$")
                       (name . "^\\*changes to .*\\*$")))
          ("Man"      (or
                       (mode . Man-mode)
                       (mode . woman-mode)))
          ("Dotfiles" (or
                        (name . "^\\.cshrc$")
                        (name . "^\\.tmux.conf$")
                        (name . "^\\.dir_colors$")
                        (name . "^\\.Xresources$")))
          ("Doc"      (mode . org-mode))
          ("Documentation" (mode . rst-mode))))))
#+end_src

#+begin_src emacs-lisp
(add-hook 'ibuffer-mode-hook 'hl-line-mode)
(add-hook 'ibuffer-mode-hook (lambda () (ibuffer-switch-to-saved-filter-groups "Default")))
#+end_src

* Desktop

#+begin_src emacs-lisp
(setq-default desktop-path (concat user-emacs-directory "desktops/"))
(require 'mf-desktop)
(global-set-key (kbd "C-c s") 'desktop-save-in-desktop-dir)
(global-set-key (kbd "C-c c") #'mf/desktop-load)
#+end_src

* Completions

#+begin_src emacs-lisp
(setq dabbrev-abbrev-char-regexp "\\sw\\|\\s_")
(setq dabbrev-abbrev-skip-leading-regexp "\\$\\|\\*\\|/\\|=")
(setq dabbrev-backward-only nil)
(setq dabbrev-case-distinction nil)
(setq dabbrev-case-fold-search t)
(setq dabbrev-case-replace nil)
(setq dabbrev-check-other-buffers t)
(setq dabbrev-eliminate-newlines t)
(setq dabbrev-upcase-means-case-search t)
#+end_src

#+begin_src emacs-lisp
(setq hippie-expand-verbose nil)
(setq hippie-expand-try-functions-list
      '(try-expand-dabbrev-visible
        try-expand-dabbrev
        try-expand-dabbrev-all-buffers
        try-expand-dabbrev-from-kill
        try-expand-list-all-buffers
        try-expand-list
        try-expand-line-all-buffers
        try-expand-line
        try-complete-file-name-partially
        try-complete-file-name
        try-expand-all-abbrevs))
#+end_src

#+begin_src emacs-lisp
(add-hook 'prog-mode-hook (lambda () (local-set-key (kbd "<backtab>") 'hippie-expand)))
#+end_src

* Bookmarks

#+begin_src emacs-lisp
(setq-default bookmark-default-file (concat user-emacs-directory "bookmarks"))
(require 'mf-bookmarks)
(global-set-key (kbd "<C-f5>") 'bookmark-bmenu-list)
(global-set-key (kbd "<f5>")   'bookmark-jump)
(with-eval-after-load "bookmark"
  (define-key bookmark-bmenu-mode-map (kbd "<mouse-2>") #'mf/bookmark-bmenu-this-window-with-mouse))
#+end_src

* Dired

#+begin_src emacs-lisp
(require 'mf-dired)
(setq-default
 dired-listing-switches "-lah1v --group-directories-first"
 dired-recursive-copies  'always
 dired-recursive-deletes 'always
 wdired-use-dired-vertical-movement nil
 wdired-allow-to-change-permissions t
 wdired-allow-to-redirect-links t)
(add-hook 'dired-load-hook (lambda () (load-library "dired-x")))
(put 'dired-find-alternate-file 'disabled nil)
#+end_src

#+begin_src emacs-lisp
(with-eval-after-load 'dired
  (define-key dired-mode-map (kbd "DEL")       (lambda () (interactive) (find-alternate-file "..")))
  (define-key dired-mode-map (kbd "RET")       'dired-find-file)
  (define-key dired-mode-map (kbd "<mouse-2>") 'dired-mouse-find-file)
  (define-key dired-mode-map (kbd "<mouse-3>") #'mf/dired-find-file-other-frame)
  (define-key dired-mode-map (kbd "f")         #'mf/find-name-dired-current)
  (define-key dired-mode-map (kbd "F")         #'mf/find-grep-dired-current)
  (define-key dired-mode-map (kbd "a")         #'mf/dired-find-file-other-frame)
  (define-key dired-mode-map (kbd "&")         #'mf/dired-open-external-file))
#+end_src

* Imenu

#+begin_src emacs-lisp
(require 'imenu)
(add-hook 'tcl-mode-hook 'imenu-add-menubar-index)
(add-hook 'python-mode-hook 'imenu-add-menubar-index)
(add-hook 'emacs-lisp-mode-hook 'imenu-add-menubar-index)
(add-hook 'c-mode-hook 'imenu-add-menubar-index)
(add-hook 'vhdl-mode-hook 'imenu-add-menubar-index)
#+end_src

#+begin_src emacs-lisp
(global-set-key (kbd "M-l") 'imenu)
(global-set-key (kbd "M-.") 'xref-find-definitions)
#+end_src

* Speedbar

#+begin_src emacs-lisp
(require 'mf-speedbar)
(with-eval-after-load 'speedbar
  (define-key speedbar-mode-map "q" #'mf/speedbar-close-window))
(global-set-key (kbd "<f6>") #'mf/speedbar-toggle)
#+end_src

- ~speedbar-get-focus~ : return focus to speedbar
- ~b~ : buffer mode
  + ~k~ : kill-buffer
  + ~r~ : revert buffer
- ~f~ : file mode
  + ~U~ : move up one directory
  + ~I~ : show information at point
  + ~C~ : Copy
  + ~R~ : Rename
  + ~D~ : Delete

#+begin_src emacs-lisp

#+end_src

* Isearch

  #+begin_src emacs-lisp
  (require 'mf-isearch)
  (setq search-highlight t)
  (setq search-whitespace-regexp ".*?")
  (setq isearch-lax-whitespace t)
  (setq isearch-regexp-lax-whitespace nil)
  (setq isearch-lazy-highlight t)
  #+end_src

  #+begin_src emacs-lisp
  (global-set-key (kbd "C-M-s") 'isearch-forward-regexp)
  (global-set-key (kbd "C-M-r") 'isearch-backward-regexp)
  (with-eval-after-load 'isearch
    (define-key isearch-mode-map (kbd "<tab>") 'isearch-complete)
    (define-key isearch-mode-map (kbd "M-o")   'isearch-occur)
    (define-key isearch-mode-map (kbd "C-<")   'isearch-yank-word-or-char)
    (define-key isearch-mode-map (kbd "C->")   'isearch-forward-symbol-at-point)
    (define-key isearch-mode-map (kbd "C-k")   #'mf/isearch-kill)
    (define-key isearch-mode-map (kbd "M-k")   #'mf/isearch-kill-to-register)
    (define-key isearch-mode-map (kbd "C-SPC") #'mf/isearch-highlight))
  #+end_src

  #+begin_src emacs-lisp
  (add-hook 'isearch-mode-hook #'mf/isearch-selection)
  #+end_src

  When using [query-]replace-regex you can use Elisp on the matched groups of the regex
  #+begin_example
  foo_\([0-9]+\)_bar → bar_\,\1 _foo
  foo_\([0-9]+\)_bar → bar_\,(* \#1 2)_foo
  bar_2_foo
  bar_4_foo
  bar_6_foo
  #+end_example

* Occur

#+begin_src emacs-lisp
(require 'mf-occur)
(global-set-key (kbd "M-s /") #'mf/multi-occur-in-matching-buffers)
(with-eval-after-load 'replace
  (define-key occur-mode-map (kbd "f") #'mf/occur-flush-lines)
  (define-key occur-mode-map (kbd "k") #'mf/occur-keep-lines))
#+end_src

* Org

#+begin_src emacs-lisp
(require 'mf-org)
(if (version= emacs-version "27.2")
    (require 'org-tempo))
#+end_src

#+begin_src emacs-lisp
(setq-default
 org-src-fontify-natively t
 org-todo-keywords '((sequence "TODO" "DO" "WAIT" "|" "DONE" ))
 org-todo-keyword-faces '(("TODO" . "red") ("DO" . "orange") ("WAIT" . "purple") ("WAIT" . "green"))
 org-startup-folded 'content
 org-hide-leading-stars t
 org-hide-emphasis-markers t
 org-startup-indented t
 org-edit-src-content-indentation 0
 org-html-validation-link nil
 org-src-preserve-indentation nil)
#+end_src

#+begin_src emacs-lisp
(org-babel-do-load-languages
 'org-babel-load-languages
 '((shell . t)
   (python . t)))
#+end_src

#+begin_src emacs-lisp
(add-hook 'org-mode-hook 'visual-line-mode)
(global-set-key (kbd "<f7>") 'org-agenda-list)
(with-eval-after-load 'org
  (define-key org-mode-map (kbd "C-c C-v C-e") #'mf/org-babel-execute-noask)
  (define-key org-mode-map (kbd "M-n") 'org-next-visible-heading)
  (define-key org-mode-map (kbd "M-p") 'org-previous-visible-heading))
#+end_src

#+begin_src emacs-lisp
(if (equal window-system 'x)
    (progn
      (add-to-list 'org-file-apps '("\\.pdf\\'"  . "evince %s"))
      (add-to-list 'org-file-apps '("\\.xslx\\'" . "libreoffice %s"))
      (add-to-list 'org-file-apps '("\\.html\\'" . "firefox %s"))))
#+end_src

* reSt

#+begin_src emacs-lisp
(require 'mf-rst)
(with-eval-after-load 'rst
  (define-key rst-mode-map (kbd "C-c C-x C-f") #'mf/rst-emphasize))
#+end_src

* Text

#+begin_src emacs-lisp
(require 'outline)
(with-eval-after-load 'outline
  (define-key outline-minor-mode-map (kbd "<tab>")     'outline-show-entry)
  (define-key outline-minor-mode-map (kbd "<backtab>") 'outline-hide-entry))
#+end_src

#+begin_src emacs-lisp
(with-eval-after-load 'view
  (define-key view-mode-map (kbd "q") 'View-kill-and-leave)
  (define-key view-mode-map (kbd "C") 'View-quit))
(add-to-list 'auto-mode-alist '("\\.rpt\\'" . view-mode))
(add-to-list 'auto-mode-alist '("\\.log.*\\'" . view-mode))
(add-to-list 'auto-mode-alist '("\\.status\\'" . view-mode))
#+end_src

* Version Control

#+begin_src emacs-lisp
;; (require 'mf-vc)
#+end_src
- vc works with git out of the box
- use ~C-x v d~ in a git repository for a global status
- then use ~m~ to mark files, ~v~ for ~next-action~ (add, commit), ~p~ to push

* Shells
** Shell

#+begin_src emacs-lisp
(if (equal system-type 'windows-nt)
    (progn
      (setq explicit-shell-file-name "C:/Program Files (x86)/Git/bin/bash.exe")
      (setq shell-file-name explicit-shell-file-name)
      (add-to-list 'exec-path "C:/Program Files (x86)/Git/bin"))
  (setq explicit-shell-file-name "/bin/bash"))
#+end_src

** Eshell

#+begin_src emacs-lisp
(setq eshell-scroll-to-bottom-on-input 'all)
(setq eshell-error-if-no-glob t)
(setq eshell-hist-ignoredups t)
(setq eshell-save-history-on-exit t)
(setq eshell-prefer-lisp-functions nil)
(setq eshell-destroy-buffer-when-process-dies t)
#+end_src

#+begin_src emacs-lisp
(add-hook 'eshell-mode-hook
  (lambda ()
    (add-to-list 'eshell-visual-commands "ssh")
    (add-to-list 'eshell-visual-commands "git")
    (add-to-list 'eshell-visual-commands "tail")))
#+end_src

** Comint

#+begin_src emacs-lisp
(add-hook 'shell-mode-hook (lambda () (local-set-key (kbd "C-l") 'comint-clear-buffer)))
(add-hook 'inferior-python-mode-hook (lambda () (local-set-key (kbd "C-l") 'comint-clear-buffer)))
(add-hook 'inferior-tcl-mode-hook (lambda () (local-set-key (kbd "C-l") 'comint-clear-buffer)))
#+end_src

* Tramp
#+begin_src emacs-lisp
(require 'mf-tramp)

(setq-default
 remote-file-name-inhibit-cache nil
 auto-revert-remote-files       t
 tramp-default-method           mf/tramp-default-method)

(add-to-list 'tramp-methods mf/tramp-plink-method)
#+end_src

* Scripts
** Tcl

#+begin_src emacs-lisp
(require 'mf-tcl)
(setq tcl-application mf/tcl-application)
(mf/tcl-update-keyword-lists)
(with-eval-after-load 'tcl
  (define-key tcl-mode-map (kbd "<tab>") #'mf/tcl-electric-tab))
(add-to-list 'auto-mode-alist '("\\.sdc\\'" . tcl-mode))
(add-to-list 'auto-mode-alist '("\\.do\\'"  . tcl-mode))
(add-to-list 'auto-mode-alist '("\\.pt\\'"  . tcl-mode))
(add-to-list 'auto-mode-alist '("synopsys_[a-z]+.setup\\'" . tcl-mode))
#+end_src

** Python

#+begin_src emacs-lisp
(require 'python)
(setq-default
 python-shell-interpreter "python3"
 python-indent 4)
(with-eval-after-load 'python
  (define-key python-mode-map (kbd "C-<") #'python-indent-shift-left)
  (define-key python-mode-map (kbd "C->") #'python-indent-shift-right))
#+end_src

#+begin_src emacs-lisp
(add-hook 'python-mode-hook
          (lambda () (setq imenu-create-index-function #'python-imenu-create-flat-index)))
#+end_src

* Custom

#+begin_src emacs-lisp
(setq custom-file (concat user-emacs-directory "custom.el"))
(unless (equal window-system nil)
  (if (file-exists-p custom-file)
      (load-file custom-file)))
#+end_src
