help:
	@echo
	@echo "Makefile to ease the deployment of dotfiles (GNU/Linux only):"
	@echo
	@echo " - deploy     : Creates symlink to .emacs.d and .tmux.conf in the HOME directory"
	@echo " - compile_el : Byte-compile .el files with emacs"
	@echo " - sync_to    : Rsync dotfiles/ to the destination directory specified in DOTFILES_SYNC_DEST"
	@echo " - sync_from  : Rsync dotfiles/ from the source directory specified in DOTFILES_SYNC_SRC"
	@echo

deploy:
	ln -rsi ${PWD}/.tmux.conf ${HOME}
	ln -rsi ${PWD}/.emacs.d ${HOME}

compile_el:
	emacs --batch --directory=.emacs.d/lisp/ --eval '(byte-recompile-directory ".emacs.d/lisp/" 0)'

sync_to:
	rsync -avzP --delete --delete-excluded --force --exclude-from=.rsync_exclude . $(DOTFILES_SYNC_DEST)

sync_from:
	rsync -avzP --delete --delete-excluded --force --exclude-from=.rsync_exclude $(DOTFILES_SYNC_SRC) .
